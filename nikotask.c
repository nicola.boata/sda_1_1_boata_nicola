#include <stdio.h>
#include <stdlib.h> //memory allocation
#include <string.h>
#include <time.h>

#define MAX_VALUE_W 100 //the maximum number of values
#define MAX_WORDS 1000 //the maximum number of words

char *x="age";//the word we search for
typedef struct Words{
    char Word[50];
    int no_of_occ;
    float freq;
}A[1000];

/*functions called outside of main and defined later in the code so that they can be reused and
called from various parts of the program*/
void SearchInInterval(struct Words A[], int NR_LINES, char *x);//searches for a specific word within a given interval defined by the first and last letters of the word 
int Binary_Search(struct Words A[], int L, int R, char *x);//performs binary search within an interval to find a specific word
int Linear_Search(struct Words A[], int L, int R, char *x);//performs linear search within an interval to find a specific word
void bubbleSort(struct Words A[], int NR_LINES);
void quickSort(struct Words A[], int low, int high);
int partition(struct Words A[], int low, int high);
void swapWords(struct Words *a, struct Words *b);

void SearchInInterval(struct Words A[], int NR_LINES, char *x){
    FILE *wf;
    wf= fopen("words_sorted.csv", "r");
    int L;
    int R;
    int nr=0;
    char first_letter_x[2];//first letter of the word we search for
    char auxL[3];
    char auxR[3];
    while(nr<1000 && fscanf(wf, "%[^,], %d, %f", A[nr].Word, &A[nr].no_of_occ, &A[nr].freq)==3){
        nr++;
    }
    strncpy(first_letter_x, x, 1);

         for(int i=0; i<nr; i++){
            strncpy(auxL, A[i].Word, 1);
            if(strcmp(auxL, first_letter_x)==0){
            L=i;
            break;
            }
         }
         if(!(L>=1 && L<1000))
         fprintf(wf,"problem in left side\n");

         for(int i=nr; i>0; i++){
            strncpy(auxR, A[i].Word, 1);
            if(strcmp(auxR, first_letter_x)==0){
            R=i;
            break;
            }
         }        
        if(!(R>=1 && R<1000))
         fprintf(wf,"problem in right side\n");

    //using binary search to find the word in the interval
    int bResult= Binary_Search(A, L, R-1, x);

    //using linear search to find the word in the interval
    int lResult= Linear_Search(A, L, R-1, x);

    //printing the results
    if (bResult!= -1){
        printf("word was found using binary search in interval [%c, %c]:\n ", L, R);
        printf("%s (occurences: %d, frequency: %.2f)\n", A[bResult].Word, A[bResult].no_of_occ, A[bResult].freq);
    }
    else if (lResult!= -1){
        printf("word was found using linear search in interval [%c, %c]:\n ", L, R);
        printf("%s (occurences: %d, frequency: %.2f)\n ", A[lResult].Word, A[lResult].no_of_occ, A[lResult].freq);
    }
    else{
        printf("word was not not found in the interval [%c, %c]\n ", L, R);
    }
}

int Binary_Search( struct Words A[], int L, int R, char *x) {
    while  (L <= R) {
        int mid=L+(R-L)/2;
        int cmp=strcmp( A[mid].Word, x);

        if (cmp== 0){
            return mid;
        }
        if (cmp< 0){
            L=mid+1;
        }
        else{
            R=mid-1;
        }
    }
    return -1; //word was not found
}

int Linear_Search(struct Words A[], int L, int R, char *x) {
    for (int i=L; i<=R; i++){
        if (strcmp(A[i].Word, x)== 0){
            return i;
        }
    }
    return -1;//word was not found
}
//bubble sort
void bubbleSort(struct Words A[], int NR_LINES) {

    for (int i=0; i<NR_LINES-1; i++) {
        for(int j=NR_LINES; j>=i; j--)
            if (A[i].no_of_occ < A[i + 1].no_of_occ) {
                // Swap the elements if they are in the wrong order
                struct Words temp= A[i];
                A[i]=A[i+1];
                A[i+1]=temp;
                
            }
        }  
}
//quick sort
void quickSort(struct Words A[], int low, int high){
    if (low < high){
        int pivot= partition(A, low, high);
        quickSort( A, low, pivot-1);
        quickSort( A, pivot+1, high);
    }
}

int partition(struct Words A[], int low, int high){
    struct Words pivot= A[high];
    int i= low-1;

    for (int j=low; j<high; j++){
        if (A[j].no_of_occ >= pivot.no_of_occ){
            i++;
            swapWords(&A[i], &A[j]);
        }
    }
    swapWords(&A[i+1], &A[high]);
    return i+1;
}

 void swapWords(struct Words *a, struct Words *b){
    struct Words temp= *a;
    *a=*b;
    *b=temp;
}

int main(){
    FILE *wf;
    FILE *of;
    char buff[MAX_VALUE_W];
    
    wf= fopen("words_sorted.csv", "r");
    if (wf == NULL){
        printf("error in opening file\n");
        exit(0);
    }

    of= fopen("output_search.txt" , "w+");
    if(of== NULL)
        return -1;

    struct Words A[1000]={0}; //initializing the array of Words
    int count=0;

    while (fgets( buff, MAX_VALUE_W, wf) != NULL) {
        char *tok= strtok(buff, ",");
        strncpy(A[count].Word, tok, 50);
        
        tok= strtok(NULL, ",");
        A[count].no_of_occ= atoi(tok);
        
        tok= strtok(NULL, "\n");
        A[count].freq= atof(tok);

        count++;
    }

    //bubble sort measure time start:
    clock_t start, end;
    double cpu_time_used;

    start= clock();
    bubbleSort(A, count);
    end= clock();
    cpu_time_used= ((double)(end-start)) / CLOCKS_PER_SEC * 1000;
    printf("bubble sort time: %f ms\n", cpu_time_used);
    //bubble sort measure time end

    // Reset array to initial state
    fseek(wf, 0, SEEK_SET);
    count=0;

    while (fgets(buff, MAX_VALUE_W, wf) != NULL){
        char *tok= strtok(buff, ",");//strtok breaks string into a series of tokens using a delimiter ,
        strncpy(A[count].Word, tok, 50);
        
        tok= strtok(NULL, ",");
        A[count].no_of_occ= atoi(tok);//atoi converts a string into an integer
        
        tok= strtok(NULL, "\n");
        A[count].freq= atof(tok);//atof cnverts floating point numbers as double values

        count++;
    }

    //quicksort measure time start:
    start= clock();
    quickSort(A, 0, count-1);
    end= clock();
    cpu_time_used= ((double)(end - start)) / CLOCKS_PER_SEC * 1000;
    printf(" quick sort time: %f ms\n", cpu_time_used);
    //quicksort measure time end

    //SearchInInterval searches in the interval determined by the given word
    SearchInInterval(A, count, x);
    return 0;
}