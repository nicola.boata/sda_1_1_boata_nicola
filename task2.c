#include <stdio.h>
#include <stdlib.h>
#include<string.h>

//de schimbat din meniu la pressuri 

struct Node 
{
    int size;
    int state;
    char address[20], ID[20], description[50];
    struct Node* next;
};

struct Node* createNode(char ID[], char address[], char description[], int size, int state){
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));//alocare dinamica nod nou
    if (newNode == NULL){
        printf("Memory allocation has failed\n");
    }
    //copiere continut in nod
    newNode->size=size;
    newNode->state=state;
    strcpy(newNode->ID,ID);
    strcpy(newNode->address,address);
    strcpy(newNode->description,description);
    newNode->next=NULL;
    return newNode;
}

void insertAtBeginning(struct Node** head, char ID[], char address[], char description[], int size, int state){
    struct Node* newNode=createNode(ID, address, description, size, state);
    newNode->next=*head;
    *head=newNode;
}

void printList(struct Node* head){
    struct Node* current=head;
    while (current!=NULL){
        char status[5];
        if(current->state==0)
        strcpy(status, "Free");
        else
        strcpy(status, "Busy");
        printf("ID:%s | Status:%s\n", current->ID, status);
        current=current->next;
    }
}

void freeList(struct Node** head){
    struct Node* current=*head;
    struct Node* nextNode;
    while (current!=NULL){
        nextNode=current->next;
        free(current);
        current=nextNode;
    }
    *head=NULL;
}

int isNodeEmpty(struct Node* head){
    if(head==NULL)
    return 1;
    else
    return 0;
}

void freeNode(struct Node* head, char* Search_ID){
    struct Node* current=head;
    struct Node* previous=NULL;
    while (current != NULL){
        if (strcmp(current->ID, Search_ID)==0){
            if (previous!= NULL){
                previous->next=current->next;
            }
            else{
                head=current->next;
            }
            free(current);
            return;
        }
        previous=current;
        current=current->next;
    }
    printf("Node with the '%s' UID was not found =(\n", Search_ID);
}


int main(){
    int size, command;
    int state=1;
    int start=0;
    int empty=0;
    char ID[20], description[50], address[20], Searched_ID[20];
    struct Node* head=NULL;
    FILE *file=fopen("citire.csv","r+");
    if(file==NULL){
        printf("\nError opening the file or the file is empty =(\n");
        return -1; 
    }
    while(start==0){//so that menu never closes
        while(fscanf(file, "%[^,],%[^,],\"%50[^\"]\",%d,%d", ID, address, description, &size, &state)==5){  
            insertAtBeginning(&head, ID, address, description, size, state);         
        }
        printf("\n ~ ~ M E N U ~ ~  \n");
        printf("\n0) Exit\n1) Display the unique IDS and the States of the nodes\n2) Free the list\n3) Enter the UID of the node that is about to be freed\n");
        scanf("%d", &command);
        if(command==0){
            start=1;
        }
        if(command==1){
            printf("\n ~ ~ L I S T  S T A T U S ~ ~  \n\n");
            if(empty==1)
                printf("\nEmpty list\n\n");
            printList(head);
        }
        if(command==2){
            printf("\n ~ ~ T H E  L I S T  I S  F R E E ~ ~ \n\n");
            empty=1;
            freeList(&head);
        }
        if(command==3){
            scanf("%s\n",Searched_ID);
            freeNode(head,Searched_ID);
            printf("\n ~ ~ T H E  N O D E  I S  F R E E ~ ~ \n\n");
        }
    }
}